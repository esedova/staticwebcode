package com.amdocs;
import static org.junit.Assert.*;
import org.junit.Test;

public class IncrementTest {

int key;
    @Test
    public void testdecreasecounterTwo() throws Exception {

        key= new Increment().decreasecounter(2);
        assertEquals("Counter_sub2", 2, key);
}
   
     @Test
    public void testdecreasecounterZero() throws Exception {

        key= new Increment().decreasecounter(0);
        assertEquals("Counter_sub0", 3, key);       
    }
   
    @Test
    public void testdecreasecounterOne() throws Exception {

        key= new Increment().decreasecounter(1);
        assertEquals("Counter_sub1", 2, key);       
    }
   
    @Test
    public void testCounter() throws Exception {

        key= new Increment().getCounter();
        assertEquals("Counter_add", 3, key);

    }
}
